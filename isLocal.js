const _ = require('lodash')
const assertLocation = require('./assertLocation.js')

module.exports = function () {
    assertLocation()
    return location.href.indexOf('http://localhost') > -1 || location.href.indexOf('file://') > -1
}