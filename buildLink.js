const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Data was expected in method.')

    if(!_.isObject(args.data))
        throw new Error('Data was expected in method in a "data" property.')

    if(!_.isString(args.data.mod))
        throw new Error('Expected a string as a mod property. Received ' + typeof args.data.mod)

    if(!_.isString(args.data.cm))
        throw new Error('Expected a string as a cm property. Received ' + typeof args.data.cm)

    if(!_.isString(args.host))
        throw new Error('A host was expected in a string to build link.')

    return args.host + '/mod/' + args.data.mod + '/view.php?id=' + args.data.cm
}